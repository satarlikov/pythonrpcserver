#!/usr/bin/env python3
import pika
import uuid
import socket
import time

class FibonacciRpcClient(object):

    def __init__(self):
        self.credentials = pika.PlainCredentials('python_app', 'Test1234')
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='rabbit', credentials=self.credentials))

        self.channel = self.connection.channel()

        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, n):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key='rpc_queue',
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=str(n))
        while self.response is None:
            self.connection.process_data_events()
        return int(self.response)


pingcounter = 0
isreachable = False
while isreachable is False and pingcounter < 20:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect(('rabbit', 5672))
        isreachable = True
    except socket.error as e:
        time.sleep(2)
        pingcounter += 1
    s.close()
if isreachable:

    fibonacci_rpc = FibonacciRpcClient()

    print(" [x] Requesting fib(20)")
    response = fibonacci_rpc.call(20)
    print(" [.] Got %r" % response)